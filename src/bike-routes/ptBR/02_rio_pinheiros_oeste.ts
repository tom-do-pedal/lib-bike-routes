import { IBikeRoutes } from "../interface";
const { default: thumbnail } = require("../../imgs/marginal_pinheiros_oeste_parque_bruno_covas.jpeg")
const { default: img01 } = require("../../imgs/ponto_estaiada_02.jpeg")
const { default: img02 } = require("../../imgs/parque_do_povo.jpeg")
const { default: img03 } = require("../../imgs/ponte_laguna.jpeg")
const { default: img04 } = require("../../imgs/chucri_zaidan.jpeg")

export default {
  routeName: 'Ciclovia Rio Pinheiros Oeste (Parque Bruno Covas) - Morumbi',
  distance: '25km',
  difficulty: 2,
  thumbnaillUrl: thumbnail,
  galleryImgs: [
    {
      url: img02,
      title: 'Parque do Povo',
      description: 'Contrasta a natureza e beleza de um parque bem cuidado com o fundo dos prédios modernos da capital.'
    }, {
      url: img01,
      title: 'Ponte estaiada',
      description: 'Do lado oeste da marginal tem um ponto bem requisitado para fotos, depois seguimos caminho passando por baixo da ponte e indo para Zona Sul.'
    },
    {
      url: img03,
      title: "Ponte Laguna",
      description: 'Saindo da ciclovia, vamos subir para a ponte Laguna, onde teremos uma linda vista do Rio Pinheiros e do Parque Burle Marx.'
    },
    {
      url: img04,
      title: "Av. Chucri Zaidan",
      description: 'Conhecida pelo seu complexo de prédios comerciais com arquitetura moderna, a avenida também tem sua fama pelos muitos comerciais de carro que são gravados lá.'
    },
  ],
  routeHighlights: [
    'Parque do Povo',
    'Parque Bruno Covas',
    'Ponte Estaiada',
    'Vista da Ponte Laguna',
    'Complexo de prédios empresariais (Av. Chucri Zaidan e Av. Berrini)',
  ],
  routePoints: {
    start: 'Parque Ibirapuera (Portão 9)',
    end: 'Parque Ibirapuera (Portão 9)',
  },
  description: 'Na marginal do rio costuma ventar bastante, o que pode dificultar um pouco a pedalada, além de atenção redobrada com o ciclistas velocistas. Porém a pista é excelente, durante o trajeto temos bases de apoio com banheiros, água filtrada, espaço para repouso e lugares para lanchar.',
  routeUrl: 'https://www.google.com.br/maps/dir/Parque+Ibirapuera+-+Port%C3%A3o+9,+Alameda+dos+Anos+Dourados+-+Vila+Mariana,+S%C3%A3o+Paulo+-+SP,+04094-050/-23.5860153,-46.6887799/-23.5887712,-46.69247/-23.6331584,-46.7188857/-23.6340865,-46.7124158/-23.5947968,-46.6897922/-23.5806093,-46.6609479/@-23.596425,-46.6919732,16.78z/data=!4m24!4m23!1m15!1m1!1s0x94ce592d137c006b:0xe41e45f7c91ea9a!2m2!1d-46.6611909!2d-23.5806701!3m4!1m2!1d-46.6897939!2d-23.5948147!3s0x94ce5747f1b4dadb:0x66100d5cb82bfa94!3m4!1m2!1d-46.6869272!2d-23.587512!3s0x94ce574053d5c353:0x1a3697a7910e43c6!1m0!1m0!1m0!1m0!1m0!1m0!3e1!5m1!1e3?hl=pt-BR'
} as IBikeRoutes