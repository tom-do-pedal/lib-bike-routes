import { IBikeRoutes } from "../interface";
const { default: thumbnail } = require("../../imgs/parque_villa_lobos_entrada.jpeg")
const { default: img01 } = require("../../imgs/paroquia_nossa_senhora_do_brasil.jpeg")
const { default: img02 } = require("../../imgs/parque_vila_lobos_volta.jpeg")
const { default: img03 } = require("../../imgs/parque_villa_lobos_biblioteca.jpeg")
const { default: img04 } = require("../../imgs/parque_portinari_roda_gigante.jpeg")

export default {
  routeName: 'Parque Villa Lobos Tour',
  distance: '20km',
  difficulty: 2,
  thumbnaillUrl: thumbnail,
  galleryImgs: [
    {
      url: img01,
      title: 'Paróquia Nossa Senhora do Brasil',
      description: 'Paróquia com uma bela arquitetura, localizada na Avenida Brasil.'
    }, {
      url: img02,
      title: 'Parque Villa Lobos',
    },
    {
      url: img03,
      title: "Biblioteca do Parque Villa Lobos",
      description: 'No parque, como um dos grande destaques, temo sua biblioteca pública, muito bem cuidada, com jogos de tabuleiro, videogame, acesso à internet... e tudo de graça, basta fazer o cadastro na hora.'
    },
    {
      url: img04,
      title: "Roda Gigante Rico, vista do parque Portinari",
      description: 'Vamos poder ver a grandeza da Roda Gigante, estaremos bem aos pés dela no nosso Tour.'
    },
  ],
  routeHighlights: [
    'Av. Brasil (Paróquia Nossa Senhora do Brasil)',
    'Av. Faria Lima (parte com mais natureza)',
    'Parque Villa Lobos e Parque Portinari (maior roda gigante da América Latina)',
  ],
  routePoints: {
    start: 'Parque Ibirapuera (Portão 9)',
    end: 'Parque Ibirapuera (Portão 9)',
  },
  recommendedDays: ["sunday", "holiday"],
  description: 'O trajeto pela Faria até o parque Villa Lobos conta com um caminho repleto de árvores, que por muitas vezes chegam a cobrir toda a ciclo via. Chegando no parque Villa Lobos iremos dar uma volta pela sua parte mais externa, faremos uma parada para apresentar a biblioteca pública do parque. O parque conta com uma excelente infraestrutura, com banheiros, água, lanches e doces para se alimentar.',
  routeUrl: 'https://www.google.com.br/maps/dir/Parque+Ibirapuera+-+Port%C3%A3o+9,+Alameda+dos+Anos+Dourados+-+Vila+Mariana,+S%C3%A3o+Paulo+-+SP,+04094-050/Parque+Villa-Lobos+-+Avenida+Professor+Fonseca+Rodrigues+-+Alto+de+Pinheiros,+S%C3%A3o+Paulo+-+State+of+S%C3%A3o+Paulo/Parque+Ibirapuera+-+Port%C3%A3o+9,+Alameda+dos+Anos+Dourados+-+Vila+Mariana,+S%C3%A3o+Paulo+-+SP,+04094-050/@-23.5544753,-46.7182355,14.02z/data=!4m25!4m24!1m5!1m1!1s0x94ce592d137c006b:0xe41e45f7c91ea9a!2m2!1d-46.6611909!2d-23.5806701!1m10!1m1!1s0x94ce56fa971a5e7d:0x6f6b8e0ec9daadd1!2m2!1d-46.7253758!2d-23.5463088!3m4!1m2!1d-46.6963074!2d-23.5599437!3s0x94ce57a4cbd09fe5:0xb015c252127ef67e!1m5!1m1!1s0x94ce592d137c006b:0xe41e45f7c91ea9a!2m2!1d-46.6611909!2d-23.5806701!3e1!5m1!1e3?hl=pt-BR'
} as IBikeRoutes