import { IBikeRoutes } from "../interface";
const { default: thumbnail } = require("../../imgs/estadio_morumbi_longe.jpeg")
const { default: img01 } = require("../../imgs/parque_do_povo.jpeg")
const { default: img02 } = require("../../imgs/jockey_club.jpeg")
const { default: img03 } = require("../../imgs/usp_torre_relogio.jpeg")
const { default: img04 } = require("../../imgs/usp_capivaras.jpeg")
const { default: img05 } = require("../../imgs/estadio_morumbi_02.jpeg")

export default {
  routeName: 'Gio Tour (Estádio Morumbi)',
  distance: '30km',
  difficulty: 3,
  thumbnaillUrl: thumbnail,
  galleryImgs: [
    {
      url: img01,
      title: 'Parque do Povo',
      description: 'Contrasta a natureza e beleza de um parque bem cuidado com o fundo dos prédios modernos da capital.'
    },
    {
      url: img02,
      title: 'Jockey Club',
      description: 'Andaremos em volta do hipódromo mais famoso da cidade de São Paulo, localizado na Cidade Jardim.',
    },
    {
      url: img03,
      title: "USP",
      description: 'Caso o passeio seja feito fora de domingos e feriádos, poderemos entrar na USP, conhecer alguns pontos e monumentos históricos da cidade universitária.'
    },
    {
      url: img04,
      title: "Raia Olímpica da USP",
      description: 'Na raia olímpica, pela sua extensão iremos ver pessoas de caiaque praticando o esporte e as simpáticas capivaras na margem do lago.'
    },
    {
      url: img05,
      title: "Estádio Morumbi",
      description: 'Sua arquiterura antiga se mostra imponente conforme vamos nos aproximando.'
    },
  ],
  routeHighlights: [
    'Parque do Povo',
    'Jockey Club de São Paulo',
    'USP (exceto domingo)',
    'Estádio do Morumbi'
  ],
  routePoints: {
    start: 'Parque Ibirapuera (Portão 9)',
    end: 'Parque Ibirapuera (Portão 9)',
  },
  recommendedDays: ['saturday'],
  description: 'Rota inspirada no caminho do dia-a-dia do instrutor Giovanni, trajeto plano, apresenta poucas subidas. Caso o passeio não seja feito aos domingos e feriados, será possível esticar a pedalada dentro da USP.',
  routeUrl: 'https://www.google.com.br/maps/dir/Parque+Ibirapuera+-+Port%C3%A3o+9,+Alameda+dos+Anos+Dourados+-+Vila+Mariana,+S%C3%A3o+Paulo+-+SP,+04094-050/-23.5947994,-46.6898285/-23.5867122,-46.6926868/-23.5667159,-46.7094913/-23.5824425,-46.7254576/-23.5975555,-46.7201728/-23.5667448,-46.7091266/-23.5869897,-46.6929282/-23.5864274,-46.6867924/-23.5806916,-46.6610223/@-23.5798272,-46.7137585,13.22z/data=!4m32!4m31!1m5!1m1!1s0x94ce592d137c006b:0xe41e45f7c91ea9a!2m2!1d-46.6611909!2d-23.5806701!1m5!3m4!1m2!1d-46.6871623!2d-23.5864148!3s0x94ce574053d5c353:0x1a3697a7910e43c6!1m0!1m5!3m4!1m2!1d-46.7093503!2d-23.5692005!3s0x94ce565353f3fbdb:0xee0ea924a902b08d!1m0!1m0!1m0!1m0!1m5!3m4!1m2!1d-46.6890762!2d-23.5941983!3s0x94ce574793d594d7:0x63ceb58a8486aaab!1m0!3e1!5m1!1e3?hl=pt-BR'
} as IBikeRoutes