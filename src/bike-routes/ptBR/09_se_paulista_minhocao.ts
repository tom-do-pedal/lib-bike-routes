import { IBikeRoutes } from "../interface";
const { default: thumbnail } = require("../../imgs/av_paulista.jpeg")
const { default: img01 } = require("../../imgs/bairro_liberdade.jpeg")
const { default: img02 } = require("../../imgs/igreja_se.jpeg")
const { default: img03 } = require("../../imgs/av_paulista_02.jpeg")
const { default: img04 } = require("../../imgs/minhocao_02.jpeg")

export default {
  routeName: 'Colinas da Central',
  distance: '25km',
  difficulty: 4,
  thumbnaillUrl: thumbnail,
  galleryImgs: [
    {
      url: img01,
      title: 'Bairro da Liberdade',
      description: 'Bairro da capital todo temático com a cultura japonesa.'
    }, {
      url: img02,
      title: 'Igreja da Sé',
      description: 'A igreja mais conhecida de São Paulo, fazendo jus com o seu tamanho e sua beleza.',
    },
    {
      url: img03,
      title: "Av. Paulista",
      description: 'Não podemos fazer esse tour pelo centro de São Paulo e esquecer da avenida mais famosa da cidade, onde iremos passar por toda sua extensão.'
    },
    {
      url: img04,
      title: "Minhocão",
      description: 'Aos domingos, assim como a Av. Paulista, o Minhocão é aberto para os pedestres e ciclistas.S'
    },
  ],
  routeHighlights: [
    'Bairro da Liberdade',
    'Volta ao redor da igreja da Sé',
    'Av. Paulista',
    'Minhocão'
  ],
  routePoints: {
    start: 'Posto Ipiranga (Rua Sena Madureira, 50)',
    end: 'Parque Ibirapuera (Portão 9)',
  },
  recommendedDays: ['sunday', 'holiday'],
  description: 'Se engana quem acha que esse trajeto será tranquilo pela sua quilometragem, todo o percurso será cheio de subidas e descidas, esteja bem preparado. Iremos passar por pontos turísticos de São Paulo, como a Igreja da Sé, Liberdade, Av Paulista e o Minhocão (que aos domingos e feriados tem espaço para descanso e lazer).',
  routeUrl: 'https://www.google.com.br/maps/dir/-23.5929381,-46.63667/-23.5523793,-46.6343897/-23.57356,-46.640616/-23.555691,-46.6627666/-23.5455454,-46.6477515/-23.5318407,-46.6602207/-23.5457315,-46.6477743/-23.5556457,-46.6625684/-23.5715443,-46.6442033/Parque+Ibirapuera+-+Port%C3%A3o+9/@-23.558464,-46.6650051,13z/data=!4m17!4m16!1m0!1m0!1m0!1m0!1m0!1m0!1m0!1m0!1m0!1m5!1m1!1s0x94ce592d137c006b:0xe41e45f7c91ea9a!2m2!1d-46.6611909!2d-23.5806701!3e1!5m1!1e3?hl=pt-BR'
} as IBikeRoutes