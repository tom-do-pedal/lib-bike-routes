import { IBikeRoutes } from "../interface";
const { default: thumbnail } = require("../../imgs/ponte_estaiada.jpeg")
const { default: img01 } = require("../../imgs/ponte_estaiada.jpeg")
const { default: img02 } = require("../../imgs/parque_do_povo.jpeg")
const { default: img03 } = require("../../imgs/roda_gigante_rico_longe.jpeg")

export default {
  routeName: 'Ciclovia Rio Pinheiros Leste',
  distance: '35km',
  difficulty: 3,
  thumbnaillUrl: thumbnail,
  galleryImgs: [
    {
      url: img02,
      title: 'Parque do Povo',
      description: 'Contrasta a natureza e beleza de um parque bem cuidado com o fundo dos prédios modernos da capital.'
    },
    {
      url: img01,
      title: 'Ponte estaiada',
      description: 'Do lado leste da marginal conseguimos chegar na parte de baixo de uma das pilastras da ponto.'
    },
    {
      url: img03,
      title: "Roda Gigante Rico",
      description: 'Na parada final ao extremo norte da ciclovia, podemos ver a maior roda gigante da América Latina.'
    },

  ],
  routeHighlights: [
    'Parque do Povo',
    'Ponte Estaiada (marginal pinheiros)',
    'Vista para a maior roda gigante da América do Sul (Roda Rico - parque Cândido Portinari)',
  ],
  routePoints: {
    start: 'Parque Ibirapuera (Portão 9)',
    end: 'Parque Ibirapuera (Portão 9)',
  },
  description: 'Na marginal do rio costuma ventar bastante, o que pode dificultar um pouco a pedalada, além de atenção redobrada com o ciclistas velocistas. Porém a pista é excelente, durante o trajeto temos bases de apoio com banheiros, água filtrada, espaço para repouso e lugares para lanchar.',
  routeUrl: 'https://www.google.com.br/maps/dir/Parque+Ibirapuera+-+Port%C3%A3o+9,+Alameda+dos+Anos+Dourados+-+Vila+Mariana,+S%C3%A3o+Paulo+-+SP,+04094-050/-23.5860153,-46.6887799/-23.5892299,-46.6916137/-23.6126575,-46.6990128/-23.5465852,-46.7328109/-23.5892302,-46.6916132/-23.5860127,-46.6887693/-23.5875324,-46.6869365/-23.5948276,-46.6898051/-23.5806093,-46.6610188/@-23.5861331,-46.7184944,13z/data=!4m37!4m36!1m15!1m1!1s0x94ce592d137c006b:0xe41e45f7c91ea9a!2m2!1d-46.6611909!2d-23.5806701!3m4!1m2!1d-46.6897939!2d-23.5948147!3s0x94ce5747f1b4dadb:0x66100d5cb82bfa94!3m4!1m2!1d-46.6869272!2d-23.587512!3s0x94ce574053d5c353:0x1a3697a7910e43c6!1m5!3m4!1m2!1d-46.6934394!2d-23.5866125!3s0x94ce57159131337f:0x5b714f31a6f3d2cd!1m0!1m0!1m0!1m0!1m0!1m5!3m4!1m2!1d-46.6885807!2d-23.5944274!3s0x94ce574793d594d7:0x332a6121f14b46c6!1m0!1m0!3e1!5m1!1e3?hl=pt-BR'
} as IBikeRoutes