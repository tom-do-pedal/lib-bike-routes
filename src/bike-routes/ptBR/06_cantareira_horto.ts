import { IBikeRoutes } from "../interface";
const { default: thumbnail } = require("../../imgs/horto_florestal_entrada.jpeg")
const { default: img01 } = require("../../imgs/allianz_parque.jpeg")
const { default: img02 } = require("../../imgs/horto_florestal_cantareira.jpeg")
const { default: img03 } = require("../../imgs/aeroporto_campo_de_marte_02.jpeg")
const { default: img04 } = require("../../imgs/sambodromo_anhembi.jpeg")

export default {
  routeName: 'Horto Florestal (Cantareira)',
  distance: '45km',
  difficulty: 5,
  thumbnaillUrl: thumbnail,
  galleryImgs: [
    {
      url: img01,
      title: 'Allianz Parque',
      description: 'Passaremos ao lado de um dos estádios brasileiros mais modernos.'
    }, {
      url: img02,
      title: 'Horto Florestal',
      description: 'Após uma longa jornada chegamos ao Horto Florestal, onde podemos aproveitar o parque que tem alguns animais soltos como patos e lagos cheios de carpas.',
    },
    {
      url: img03,
      title: "Campo de Marte",
      description: 'Passaremos ao lado do 1º aeroporto de São Paulo, porém hoje não conta com vôos, mas sim, apenas com vôos de pequeno porte.'
    },
    {
      url: img04,
      title: "Sambódromo do Anhembi",
      description: 'Famoso sambódromo da cidade de São Paulo, onde acontecem os desfiles das escolas de samba da categoria de elite da capital.'
    },
  ],
  routeHighlights: [
    'Allianz Parque',
    'Horto Florestal (Cantareira)',
    'Aeroporto Campo de Marte',
    'Sambódromo do Anhembi',
  ],
  routePoints: {
    start: 'Parque Ibirapuera (Portão 9)',
    end: 'Parque Ibirapuera (Portão 9)',
  },
  recommendedDays: ["sunday", "holiday"],
  description: 'A rota é bem pesada, cheia de subidas em toda sua extensão, ótima para um treino bem pesado. Ao chegar na entrada do horto, iremos fazer o almoço e usaremos o passeio no horto para fazer a digestão para não passar mal no retorno. A volta será feita por outro caminho passando em frente ao Campo de Marte e o Sambódromo.',
  routeUrl: 'https://www.google.com.br/maps/dir/Parque+Ibirapuera+-+Port%C3%A3o+9,+Alameda+dos+Anos+Dourados+-+Vila+Mariana,+S%C3%A3o+Paulo+-+SP,+04094-050/-23.5210005,-46.667413/-23.5091557,-46.6801972/-23.5059134,-46.6689235/-23.4726338,-46.626329/Portaria+Horto+Florestal,+R.+do+Horto,+936+-+Horto+Florestal,+S%C3%A3o+Paulo+-+SP,+02377-000/-23.4866153,-46.63982/-23.5032637,-46.6248308/-23.5157762,-46.6522405/-23.5795454,-46.6614924/@-23.5215804,-46.6684606,14.3z/data=!4m32!4m31!1m5!1m1!1s0x94ce592d137c006b:0xe41e45f7c91ea9a!2m2!1d-46.6611909!2d-23.5806701!1m0!1m5!3m4!1m2!1d-46.6800562!2d-23.5050814!3s0x94cef822ad6a620f:0xdffa9ece6137b761!1m0!1m5!3m4!1m2!1d-46.6309211!2d-23.467379!3s0x94cef6fe3ed57a73:0x8739eb63ee6fabb6!1m5!1m1!1s0x94cef74350bddf13:0xb31591586594dfe0!2m2!1d-46.6334797!2d-23.4591137!1m0!1m0!1m0!1m0!3e1!5m1!1e3?hl=pt-BR'
} as IBikeRoutes