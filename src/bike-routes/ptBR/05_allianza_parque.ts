import { IBikeRoutes } from "../interface";
const { default: thumbnail } = require("../../imgs/allianz_parque_02.jpeg")
const { default: img01 } = require("../../imgs/allianz_parque.jpeg")
const { default: img02 } = require("../../imgs/predio_federacao_paulista_futebol.jpeg")

export default {
  routeName: 'Allianz Parque',
  distance: '20km',
  difficulty: 3,
  thumbnaillUrl: thumbnail,
  galleryImgs: [
    {
      url: img01,
      title: 'Allianz Parque',
      description: 'Passaremos ao lado de um dos estádios brasileiros mais modernos.'
    }, {
      url: img02,
      title: 'Prédio da Federação Paulista',
      description: 'Próximo ao estádio, poderemos ver o prédio da FPF, onde hoje temos uma foto em homenagem ao Pelé e na entrada, as bandeiras de todas as equipes da 1ª divisão do campeonato Paulista.',
    },
  ],
  routeHighlights: [
    'Allianz Parque',
    'Prédio da Federação Paulista',
  ],
  routePoints: {
    start: 'Parque Ibirapuera (Portão 9)',
    end: 'Parque Ibirapuera (Portão 9)',
  },
  recommendedDays: ["sunday", "holiday"],
  description: 'A rota com bastante subida, boa para quem quer fazer um belo treino de pernas. Passaremos do lado do estádio Allianz Parque e depois iremos em frente ao prédio da Federação Paulista.',
  routeUrl: 'https://www.google.com.br/maps/dir/Parque+Ibirapuera+-+Port%C3%A3o+9,+Alameda+dos+Anos+Dourados+-+Vila+Mariana,+S%C3%A3o+Paulo+-+SP,+04094-050/-23.5210005,-46.667413/-23.529773,-46.6770704/-23.5801869,-46.6611036/@-23.550842,-46.7060789,13z/data=!4m11!4m10!1m5!1m1!1s0x94ce592d137c006b:0xe41e45f7c91ea9a!2m2!1d-46.6611909!2d-23.5806701!1m0!1m0!1m0!3e1!5m1!1e3?hl=pt-BR'
} as IBikeRoutes