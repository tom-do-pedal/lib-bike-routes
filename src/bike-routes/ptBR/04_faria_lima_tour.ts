import { IBikeRoutes } from "../interface";
const { default: thumbnail } = require("../../imgs/faria_lima_tour.jpeg")
const { default: img01 } = require("../../imgs/paroquia_nossa_senhora_do_brasil.jpeg")
const { default: img02 } = require("../../imgs/predio_google.jpeg")
const { default: img03 } = require("../../imgs/baleia_metalica.jpeg")
const { default: img04 } = require("../../imgs/parque_das_bicicletas_pulp_track.jpeg")

export default {
  routeName: 'Moderna Faria Lima e Parque das Bicicletas',
  distance: '15km',
  difficulty: 1,
  thumbnaillUrl: thumbnail,
  galleryImgs: [
    {
      url: img01,
      title: 'Paróquia Nossa Senhora do Brasil',
      description: 'Paróquia com uma bela arquitetura, localizada na Avenida Brasil.'
    }, {
      url: img02,
      title: 'Prédio do Google',
      description: 'Sem nada que referente à marca que indique, mas vamos passar pelo prédio espelhado do Google, com um jardim de gramado sintético onde é possível parar para relaxar um pouco.',
    },
    {
      url: img03,
      title: "Baleia Metálica",
      description: 'Próximo ao prédio do Google temos o Edifício Baleia, onde podemos ver uma grande baleia metálica e até entrar dentro dela.'
    },
    {
      url: img04,
      title: "Parque das Bicicletas",
      description: 'Para os quem quiser adicionar um dose de adrenalina, temos a pista de Pulp Track, logo na entrada do parque.'
    },
  ],
  routeHighlights: [
    'Av. Brasil (Paróquia Nossa Senhora do Brasil)',
    'Prédio do Google (Faria Lima)',
    'Baleia Metálica (Edifício Baleia - Faria Lima)',
    'Parque das Bicicletas com Pulp Track'
  ],
  routePoints: {
    start: 'Parque Ibirapuera (Portão 9)',
    end: 'Parque Ibirapuera (Portão 9)',
  },
  recommendedDays: ["sunday", "holiday"],
  description: 'Uma pedalada voltada para quem é iniciante e quer começar a desbravar a cidade, o trajeto pela Faria Lima irá mostrar prédios com arquiteturas únicas. Faremos paradas para fotos tanto no prédio da Google (dependendo da época do ano, pode estar enfeitado) quanto no edifício Baleia. Antes de retornar para o Ibirapuera, passaremos pelo parque das bicicletas.',
  routeUrl: 'https://www.google.com.br/maps/dir/Parque+Ibirapuera+-+Port%C3%A3o+9,+Alameda+dos+Anos+Dourados+-+Vila+Mariana,+S%C3%A3o+Paulo+-+SP,+04094-050/-23.5709483,-46.6906023/-23.5962775,-46.6637825/Parque+das+Bicicletas+-+Alameda+Ira%C3%A9+-+Moema,+S%C3%A3o+Paulo+-+State+of+S%C3%A3o+Paulo/-23.5806738,-46.6611137/@-23.5863316,-46.6739155,13.38z/data=!4m22!4m21!1m10!1m1!1s0x94ce592d137c006b:0xe41e45f7c91ea9a!2m2!1d-46.6611909!2d-23.5806701!3m4!1m2!1d-46.6906023!2d-23.5709483!3s0x94ce570b3975b47b:0x29d0047f8c4fd4ad!1m0!1m0!1m5!1m1!1s0x94ce5a1ba30d3529:0x926358f73e88fc45!2m2!1d-46.6566846!2d-23.5995116!1m0!3e1!5m1!1e3?hl=pt-BR'
} as IBikeRoutes