import { IBikeRoutes } from "../interface";
const { default: thumbnail } = require("../../imgs/zona_oeste.jpeg")
const { default: img01 } = require("../../imgs/parque_villa_lobos_entrada.jpeg")
const { default: img02 } = require("../../imgs/allianz_parque.jpeg")

export default {
  routeName: 'Volta Zona Oeste',
  distance: '30km',
  difficulty: 4,
  thumbnaillUrl: thumbnail,
  galleryImgs: [
    {
      url: img02,
      title: 'Parque Villa Lobos',
      description: 'Um dos parques com melhor infraestrutura da cidade, sempre recebe eventos nos finais de semana. Para quem curte parques, esse é uma das melhores pedidas.',
    },
    {
      url: img01,
      title: 'Allianz Parque',
      description: 'Passaremos ao lado de um dos estádios brasileiros mais modernos.'
    },
  ],
  routeHighlights: [
    'Parque Villa Lobos',
    'Lapa (residencial nobre)',
    'Allianz Parque',
  ],
  routePoints: {
    start: 'Parque Ibirapuera (Portão 9)',
    end: 'Parque Ibirapuera (Portão 9)',
  },
  description: 'Trajeto com várias subidas bem pesadas. Passaremos pelo Parque Villa Lobos, até o extremo da zona Oeste, onde vamos em direção à Lapa, passando por lugares tranquilos, com casas nobres (lembrando cidades do interior) até chegar ao Allianz Parque onde iremos retornar para o Parque Ibirapuera.',
  routeUrl: 'https://www.google.com.br/maps/dir/Parque+Ibirapuera+-+Port%C3%A3o+9,+Alameda+dos+Anos+Dourados+-+Vila+Mariana,+S%C3%A3o+Paulo+-+SP,+04094-050/-23.5293741,-46.7384528/-23.5249158,-46.7213778/-23.5349886,-46.6744284/-23.5808034,-46.6611509/@-23.5468347,-46.7164367,12.99z/data=!4m12!4m11!1m5!1m1!1s0x94ce592d137c006b:0xe41e45f7c91ea9a!2m2!1d-46.6611909!2d-23.5806701!1m0!1m0!1m0!1m0!3e1!5m1!1e3?hl=pt-BR'
} as IBikeRoutes