type DifficultyRange = 1 | 2 | 3 | 4 | 5
type RecommendedDays = "all" | "weekend" | "holiday" | "saturday" | "sunday"

export interface IImage {
  url: string;
  title?: string;
  description?: string;
}
export interface IBikeRoutes {
  routeName: string;
  distance: string;
  thumbnaillUrl?: string;
  galleryImgs?: IImage[];
  difficulty: DifficultyRange;
  routeHighlights: string[];
  routePoints: {
    start: string,
    end: string,
    others?: string | string[],
  },
  recommendedDays?: RecommendedDays[];
  description: string;
  routeUrl: string;
}