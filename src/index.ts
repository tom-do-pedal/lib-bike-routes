import * as bikeRoutesPTBR from './bike-routes/ptBR'
import { IBikeRoutes } from './bike-routes/interface'

interface RouteItem extends IBikeRoutes {
  id: string;
}

export const getBikeRoutesList = (searchBy?: string) => {
  // @ts-ignore
  const bikeRoutesEntries = Object.entries(bikeRoutesPTBR) as [string, IBikeRoutes][]
  const filteredList = (searchBy && searchBy.length > 3 ?
    bikeRoutesEntries.filter(([_, content]) => {
      const contentStringifyLower = JSON.stringify(content).toLocaleLowerCase()
      const searchByLower = searchBy.toLocaleLowerCase()

      // @ts-ignore
      return contentStringifyLower.includes(searchByLower)
    }) : bikeRoutesEntries) as [string, IBikeRoutes][]

  // @ts-ignore
  const list = filteredList.map(([keys, values]: [string, IBikeRoutes]) => ({ id: keys, ...values })) as RouteItem[]
  const total = list.length
  return {
    total,
    list,
  }
}

export const getBikeRoutesByDifficult = () => {
  // @ts-ignore
  const list = Object.entries(bikeRoutesPTBR)
    .reduce((routesByDifficult: Array<RouteItem[]>, [keys, values]: [string, IBikeRoutes]) => {
      const difficult = values.difficulty
      if (!routesByDifficult[difficult - 1]) routesByDifficult[difficult - 1] = [];
      routesByDifficult[difficult - 1].push({
        id: keys, ...values
      });
      return routesByDifficult
    }, [] as Array<RouteItem[]>)
  return {
    list,
  }
}