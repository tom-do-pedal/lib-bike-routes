const fs = require('fs');

const rootDir = './src/bike-routes/'

fs.readdir(rootDir, (err, dirContent) => {
  dirContent.filter(content => !content.includes(".ts")).forEach(folder => {
    const languageFolder = rootDir + folder;

    const existsIndex = fs.existsSync(languageFolder + '/index.ts')
    existsIndex && fs.rmSync(languageFolder + '/index.ts')

    fs.readdir(languageFolder, (err, bikeRouteFilesName) => {
      const parsedContent = bikeRouteFilesName
        .map(name => {
          const fileWithouExtension = name.slice(0, -3)
          return `export { default as ${fileWithouExtension.replace(/^\d*_/, '')} } from "./${fileWithouExtension}";`
        })
        .join("\n")

      fs.writeFile(languageFolder + `/index.ts`, parsedContent, {}, () => { })
    })

    // const module = import('./' + file).then(m =>
    //   m.callSomeMethod()
    // )
    // or const module = await import('file')
  })
});