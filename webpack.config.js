const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");

const deps = require("./package.json").dependencies;
module.exports = (env) => ({
  output: {
    publicPath: env.production ?
      "https://tom-do-pedal-bike-routes-lib.netlify.app/" :
      "http://localhost:8888/",
  },

  resolve: {
    extensions: [".ts", ".tsx", ".jsx", ".js", ".json"],
  },

  devServer: {
    port: 8888,
    historyApiFallback: true,
  },

  module: {
    rules: [
      {
        test: /\.m?js/,
        type: "javascript/auto",
        resolve: {
          fullySpecified: false,
        },
      },
      {
        test: /\.(ts|tsx|js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
        },
      },
      {
        test: /\.(gif|png|jpe?g|svg)$/i,
        use: [
          'file-loader',
          {
            loader: 'image-webpack-loader',
            options: {
              bypassOnDebug: true, // webpack@1.x
              disable: true, // webpack@2.x and newer
            },
          },
        ],
      }
    ],
  },

  plugins: [
    new ModuleFederationPlugin({
      name: "lib_bike_routes",
      filename: "remoteEntry.js",
      remotes: {},
      exposes: {
        "./BikeRoutesLib": "./src/index"
      },
      shared: {
        ...deps,
      },
    }),
  ],
});
